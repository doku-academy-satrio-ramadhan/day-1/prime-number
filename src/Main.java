public class Main {
    public static boolean isPrimeNumber(int number){

        if(number <= 1){
            return false;
        }

        if(number % 2 == 0 && number != 2){
            return false;
        }

        int sumDigit = getSumDigit(number);

        if(sumDigit % 3 == 0 && number != 3){
            return false;
        }

        if(number % 5 == 0 && number != 5){
            return false;
        }

        return true;
    }

    private static int getSumDigit(int number) {
        int temp = number;
        int sumDigit = 0;
        while(temp != 0){
            sumDigit += temp % 10;
            temp /= 10;
        }
        return sumDigit;
    }

    public static void main(String[] args) {
        for(int i = 1; i <= 50; i++){
            System.out.println(i + " " + isPrimeNumber(i));
        }
    }
}